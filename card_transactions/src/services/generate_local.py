def generate_local(template):
    file_name = "index.html"
    with open(file_name, "w") as file:
        file.write(template)
    print(f"Archivo HTML '{file_name}' creado exitosamente.")
