import boto3
from botocore.exceptions import ClientError


def send_email(template, email):
    print("Enviando email...")
    ses_client = boto3.client("ses", region_name="us-west-2")
    from_email = "ceedeliz@gmail.com"
    to_email = email if email is not None else "ceedeliz@gmail.com"
    subject = "Reporte Anual de Stori"
    body_text = template

    try:
        response = ses_client.send_email(
            Source=from_email,
            Destination={"ToAddresses": [to_email]},
            Message={
                "Subject": {"Data": subject},
                "Body": {"Html": {"Data": body_text}},
            },
        )
        return ("Correo enviado! ID del mensaje:", response["MessageId"])
    except ClientError as e:
        return ("Error al enviar el correo:", e.response["Error"]["Message"])
