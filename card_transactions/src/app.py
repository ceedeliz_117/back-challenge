from src.functions.balances import get_balance_report
from src.functions.generate_report import generate_report_email
from src.functions.get_data import get_transaction_data
from src.functions.manage_transaction import insert_transaction
from src.functions.manage_user import update_email
from src.services.generate_local import generate_local
from src.services.send_report import send_email


def main(event):
    local = event.get("local", False)
    email = event.get("email", None)
    payload = event.get("payload", {})

    transaction_data = get_transaction_data()

    if email is not None and len(email) != 0:
        update_email(email)

    if len(payload) > 0:
        insert_transaction(payload)

    balance_report = get_balance_report(transaction_data)
    print("balance_report")
    print(balance_report)
    report_email = generate_report_email(balance_report)

    response = send_email(report_email, email)

    if local:
        response = generate_local(report_email)
    return response
