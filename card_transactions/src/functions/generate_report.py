def generate_report_email(balance_report):
    total_balance = balance_report["total_balance"]
    credit_average = balance_report["credit_average"]
    debit_average = balance_report["debit_average"]
    months_balance = balance_report["months_balance"]
    header = """
    <!DOCTYPE html>
    <html>
    <head>
    <title>Annual Transaction Report</title>
        <style>
            body { margin: 0 auto; font-family: Arial, sans-serif; min-width: 768; max-width: 1024px;}
            .header { background-color: #f2f2f2; padding: 10px; text-align: center; }
            .footer { background-color: #f2f2f2; padding: 10px; text-align: center; }
        </style>
    </head> """
    body = f"""
    <body>

        <div class="header">
            <img src="https://cuenta.storicard.com/images/desktop/logo-footer.png" alt="Logo">
        <h1>Annual Transaction Report</h1>
        </div>

        <div class="body-content">
          <p>Dear user,</p>
            <p>We are pleased to send you the annual report of all your transactions made with credit and debit cards linked to your account. This report details all your financial operations from the last year, providing you with a clear view of your expenses and income.</p>
            <p>We believe this information is essential to help you plan your finances better and make informed decisions in the future.</p>
            <p>If you have any questions or inquiries, do not hesitate to contact us.</p>
            <br>
            <br>
            <br>
            <p>Total balance is {total_balance}</p>
            <p>Average debit amount: {credit_average}</p>
            <p>Average credit amount: {debit_average}</p>
        """
    list = ""
    for month, balance in months_balance.items():
        list += f"<p>Number of transactions in {month}: {balance}</p>"
    footer = """
        </div>

        <div class="footer">
        <p>Best regards,</p>
        <p>Your Team at Stori</p>
        </div>

    </body>
    </html>

    """
    template = header + body + list + footer
    print("Reporte de email generado")
    return template
