from src.db.db_initializer import init_db
from src.models.user import Transaction


def get_transaction_data():
    session = init_db()
    transactions = session.query(Transaction).all()
    transaction_data = []

    for transaction in transactions:
        print(
            f"ID: {transaction.id}, Name: {transaction.transaction}, Email: {transaction.user.email}, User id: {transaction.user.name}"
        )
        transaction_info = {
            "id": transaction.id,
            "transaction": transaction.transaction,
            "transaction_date": transaction.transaction_date,
            "status": transaction.status,
            "user": transaction.user.email if transaction.user else None,
            "email": transaction.user.name if transaction.user else "No user",
        }
        transaction_data.append(transaction_info)
    session.close()
    return transaction_data
