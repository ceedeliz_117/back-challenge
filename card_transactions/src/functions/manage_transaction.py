import datetime

from src.db.db_initializer import init_db
from src.repositories.transaction_repository import TransactionRepository


def insert_transaction(transaction_data):
    session = init_db()
    trans_repo = TransactionRepository(session)

    transaction_defaults = {
        "transaction": "+50",
        "transaction_date": datetime.datetime.now(),
        "reference": "Default Reference",
        "status": "Default Status",
        "user_id": 1,
    }
    transaction_info = {**transaction_defaults, **transaction_data}
    inserted_transaction = trans_repo.create_transaction(transaction_info)
    if inserted_transaction:
        print(f"Transacción insertada: {inserted_transaction.transaction}")
    else:
        print("Ocurrio un error.")

    session.close()
