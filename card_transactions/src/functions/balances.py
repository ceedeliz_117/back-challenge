from src.functions.constants import MONTHS


def get_months_balance(months_transactions):
    months_balance = {}
    months_list = MONTHS

    for i in range(0, len(months_transactions)):
        if months_transactions[i] > 0:
            months_balance[months_list[i]] = months_transactions[i]
    return months_balance


def get_balance_report(data):
    print("data")
    print(data)
    total_balance = 0.0
    credit_sum = 0.0
    debit_sum = 0.0
    credit_tr = 0
    debit_tr = 0
    months = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    months_transactions = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    for transaction in data:
        amount = float(transaction["transaction"])
        date_obj = transaction["transaction_date"]
        month = int(date_obj.month) - 1
        total_balance += amount

        months[month] = months[month] + amount
        months_transactions[month] += 1

        if "+" in transaction["transaction"]:
            credit_sum += amount
            credit_tr += 1
        elif "-" in transaction["transaction"]:
            debit_sum += amount
            debit_tr += 1

    credit_avg = credit_sum / credit_tr if credit_tr > 0 else 0
    debit_avg = debit_sum / debit_tr if debit_tr > 0 else 0
    months_balance = get_months_balance(months_transactions)
    print("months")
    print(months)
    print(months_transactions)
    return {
        "total_balance": total_balance,
        "credit_average": credit_avg,
        "debit_average": debit_avg,
        "months_balance": months_balance,
    }
