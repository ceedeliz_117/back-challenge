from src.db.db_initializer import init_db
from src.repositories.user_repository import UserRepository


def update_email(new_email):
    session = init_db()
    user_repo = UserRepository(session)
    updated_user = user_repo.update_user_email(2, new_email)
    if updated_user:
        print(f"Correo electrónico del usuario actualizado: {updated_user.email}")
    else:
        print("Ocurrio un error.")
