from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.orm import relationship
from src.db.base import Base
from src.models.transaction import Transaction


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)
    is_active = Column(Boolean)

    transactions = relationship(Transaction, back_populates="user")
