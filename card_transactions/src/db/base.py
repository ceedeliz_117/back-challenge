from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

DATABASE_URL = "sqlite:///./temp.db"
engine = create_engine(DATABASE_URL, echo=True)

Base = declarative_base()
