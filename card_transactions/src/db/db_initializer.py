import logging
import os
import requests
import boto3
from botocore.exceptions import ClientError, NoCredentialsError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.db.base import Base

# Configuración de logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

# Configuración de S3 y SQLite
s3_client = boto3.client("s3")

# Configuración de SQLite
BUCKET_NAME = "ceedeliz"
DB_FILE_NAME = "database.db"
LOCAL_DB_PATH = "/tmp/" + DB_FILE_NAME
PUBLIC_FILE_URL = f'https://{BUCKET_NAME}.s3.amazonaws.com/{DB_FILE_NAME}'


def download_db_from_s3():
    try:
        if not os.path.exists(LOCAL_DB_PATH):
            response = requests.get(PUBLIC_FILE_URL)
            if response.status_code == 200:
                with open(LOCAL_DB_PATH, 'wb') as f:
                    f.write(response.content)
                logger.info("Base de datos descargada de S3.")
            else:
                logger.error(f"Error al descargar la base de datos: HTTP {response.status_code}")
        else:
            logger.info("La base de datos ya existe en el sistema de archivos temporal.")
    except Exception as e:
        logger.error(f"Error al descargar la base de datos de S3: {e}")

def upload_db_to_s3():
    try:
        s3_client.upload_file(LOCAL_DB_PATH, BUCKET_NAME, DB_FILE_NAME)
        logger.info("Base de datos subida a S3.")
    except (NoCredentialsError, ClientError) as e:
        logger.error(f"Error al subir la base de datos a S3: {e}")


def init_db():
    download_db_from_s3()
    engine = create_engine(f"sqlite:///{LOCAL_DB_PATH}", echo=True)
    Base.metadata.create_all(engine)

    Session = sessionmaker(bind=engine)
    session = Session()

    return session
