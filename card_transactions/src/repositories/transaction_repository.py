# repositories/transaction_repository.py

from sqlalchemy.orm import Session
from src.models.transaction import Transaction


class TransactionRepository:
    def __init__(self, session: Session):
        self.session = session

    def create_transaction(self, transaction_data):
        new_transaction = Transaction(
            transaction=transaction_data["transaction"],
            transaction_date=transaction_data["transaction_date"],
            reference=transaction_data["reference"],
            status=transaction_data["status"],
            user_id=transaction_data["user_id"],
        )
        self.session.add(new_transaction)
        self.session.commit()
        return new_transaction

    def get_transactions_by_user(self, user_id):
        return (
            self.session.query(Transaction).filter(Transaction.user_id == user_id).all()
        )
