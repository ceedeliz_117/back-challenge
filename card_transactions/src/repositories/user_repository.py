from sqlalchemy.orm import Session
from src.models.user import User


class UserRepository:
    def __init__(self, session: Session):
        self.session = session

    def get_user_by_id(self, user_id):
        return self.session.query(User).filter(User.id == user_id).first()

    def create_user(self, name, email, is_active):
        new_user = User(name=name, email=email, is_active=is_active)
        self.session.add(new_user)
        self.session.commit()
        return new_user

    def update_user_email(self, user_id, new_email):
        user = self.session.query(User).filter(User.id == user_id).first()
        if user:
            user.email = new_email
            self.session.commit()
            return user
        else:
            return None
