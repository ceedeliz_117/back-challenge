import sys
from os.path import abspath, dirname

current_dir = dirname(abspath(__file__))
sys.path.insert(0, current_dir)

from src.app import main


def lambda_handler(event, context):
    response = main(event)

    # return {"statusCode": 200, "body": json.dumps("Transacción procesada con éxito")}
    return response
