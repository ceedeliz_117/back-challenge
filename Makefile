SHELL=/bin/bash
export LAMBDA?=card_transactions
export REPO=lambda-${LAMBDA}
export ENV?=dev
export ECR_URL=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com

VENV_NAME?=venv
VENV_ACTIVATE=$(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python3

install:
	@echo "Installing Python dependencies"
	@python3 -m venv $(VENV_NAME)
	@source $(VENV_ACTIVATE); pip3 install -r requirements.txt;

setup:
	@if [ ! -f .env ]; then cp .env.mock .env; fi;
	@make install;

tests:
	@echo "Running Python tests"
	@pytest;

build-package:
	@echo "Building Lambda deployment package"
	@mkdir -p package
	@. $(VENV_ACTIVATE); pip3 install --target ./package -r requirements.txt;
	@cp card_transactions/handler.py ./handler.py
	@cp -r card_transactions/src ./src
	@zip -r ${LAMBDA}-deployment-package.zip handler.py src
	@rm handler.py
	@rm -r src
	@cd package; zip -rg ../${LAMBDA}-deployment-package.zip .

apply-terraform:
	@( \
		cd terraform;  \
		terraform apply -auto-approve; \
	)

validate-tf:
	@( \
		cd terraform;  \
		terraform init; \
		printf "\033[0;34mValidating terraform...\034\n"; \
		terraform validate; \
	)

validate-terraform: validate-tf
	@printf "\033[0;34mGenerating execution plan updates...\034\n";
	@(cd terraform/ && terraform plan);

invoke-lambda-local:
	@echo "Invoke AWS lambda from local machine"
	@python3 card_transactions/src/services/invoke_local.py

.PHONY: install setup tests build-package validate-tf validate-terraform invoke-local

fast-lint:
	@bash ./scripts/fast-lint.sh

fast-lint-fix:
	@bash ./scripts/fast-lint.sh --fix

lint-fix:
	@( \
		black . --exclude=venv; \
		isort --force-single-line-imports --quiet --apply -l=250 .; \
		autoflake --recursive --exclude venv --in-place --expand-star-imports --remove-all-unused-imports ./; \
		isort --quiet --apply .; \
	)
local-run:
	@serverless invoke local -f processTransaction --path event.json