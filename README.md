# AWS Lambda Transaction Balance Projec

## Description

This project is an AWS Lambda function implemented in Python, designed to calculate the balance of financial transactions. It retrieves transaction data from a database and, upon completion, sends a summary via email.

### Sample Transaction Data

```Copy code
[
    {'id': 1, 'transaction': '+60.5', 'transaction_date': datetime.datetime(2023, 10, 25, 1, 36, 59, 366249), 'status': 'Completed', 'user': 'ceedeliz@gmail.com', 'email': 'Cesar Villeda'},
    ...
]
```

### Local Execution

- Ensure you have Serverless Framework installed.
- Set up your AWS credentials.

### To install the Serverless Framework, follow these steps:

- Install Node.js: Make sure you have Node.js installed (version 10.x or later). You can download it from Node.js website.

- Install Serverless via npm: Run the following command in your terminal:

```bash
npm install -g serverless
```

- Configure AWS credentials: Configure your AWS credentials either by using the AWS CLI or by creating an ~/.aws/credentials file manually. More details can be found in the official documentation.

### Initial Setup

- Initialize the project:

```bash
make setup
```

- Activate venv:

```bash
source venv/bin/activate
```


To execute the function locally:

```Note: To run it locally, it is necessary to update the back-challenge/card_transactions/src/db/db_initializer.py path. change the LOCAL_DB_PATH variable, it would look like this: LOCAL_DB_PATH = "./" + DB_FILE_NAME```

(console)


```bash
make invoke-lambda-local: To invoke the Lambda function locally.

```

Another way to try the lambda could be:

```bash
serverless invoke local -f processTransaction --path event.json
```


> Where event.json contains the necessary parameters for testing.


## Example of event.json

```bash
{
    "local": true,
    "email": "ceedeliz@gmail.com",
    "payload": {
        "transaction": "+10",
        "reference": "RefTest",
        "status": "In process"
    }
}

```

```Note: Sending event.json is not mandatory, and the email will update automatically. The local flag indicates if an email sample will be generated on the local machine.```


```Note: AWS accounts are not included. You will need to add your own AWS credentials.```

## Notification to the Project Creator

It is important to notify the project creator when setting up the email service, as verification is required to use the free email service.


