from unittest.mock import MagicMock

from card_transactions.src.db import db_initializer
from card_transactions.src.functions.balances import get_balance_report
from card_transactions.src.functions.get_data import get_transaction_data


def test_get_transaction_data(monkeypatch):
    fake_transactions = [
        MagicMock(
            id=1,
            transaction="Compra",
            transaction_date="2023-01-01",
            status="Completa",
            user=MagicMock(email="user1@example.com", name="User 1"),
        ),
    ]

    session_mock = MagicMock()
    session_mock.query().all.return_value = fake_transactions
    monkeypatch.setattr(
        db_initializer, "init_db", lambda: session_mock
    )  # Corrige esta línea

    result = get_transaction_data()
    assert len(result) is not None


def test_get_balance_report():
    sample_data = [
        {"transaction": "+100.0", "transaction_date": MagicMock(month=1)},
        {"transaction": "-50.0", "transaction_date": MagicMock(month=2)},
    ]
    result = get_balance_report(sample_data)
    assert isinstance(result, dict), "El resultado debe ser un diccionario"

    assert (
        "total_balance" in result
    ), "La clave 'total_balance' debe estar en el resultado"
    assert isinstance(
        result["total_balance"], float
    ), "El 'total_balance' debe ser un float"

    # Credit Average
    assert (
        "credit_average" in result
    ), "La clave 'credit_average' debe estar en el resultado"
    assert isinstance(
        result["credit_average"], float
    ), "El 'credit_average' debe ser un float"

    # Debit Average
    assert (
        "debit_average" in result
    ), "La clave 'debit_average' debe estar en el resultado"
    assert isinstance(
        result["debit_average"], float
    ), "El 'debit_average' debe ser un float"

    # Months Balance
    assert (
        "months_balance" in result
    ), "La clave 'months_balance' debe estar en el resultado"
    assert isinstance(
        result["months_balance"], dict
    ), "El 'months_balance' debe ser un diccionario"

    # Asegúrate de que los datos no sean nulos
    assert result["total_balance"] is not None, "'total_balance' no debería ser nulo"
    assert result["credit_average"] is not None, "'credit_average' no debería ser nulo"
    assert result["debit_average"] is not None, "'debit_average' no debería ser nulo"
    assert result["months_balance"] is not None, "'months_balance' no debería ser nulo"
