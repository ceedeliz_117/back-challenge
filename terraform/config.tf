### DON'T MODIFY THIS FILE ###

provider "aws" {
  default_tags {
    tags = {
      managedBy = "terraform"
      created_by = "ceedeliz"
      Inventory = "Lambda"
      project_version = "1.0.0"  
      owner = "your.email@example.com"
      repo = "https://gitlab.com/ceedeliz_117/back-challenge"
      squad = "" 
      team = ""
    }
  }
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "ceedeliz"  
    key    = "lambdas/card_transactions/terraform.tfstate"
    region = "us-west-2"
  }
}

#data "terraform_remote_state" "global" {
#  backend = "s3"
#  config = {
#    bucket = "ceedeliz" 
#    key    = "lambdas/card_transactions/terraform.tfstate"
#    region = "us-west-2"
#  }
#}
