module "card_transactions_lambda" {
  source  = "terraform-aws-modules/lambda/aws"
  version = "3.0.0"

  function_name = "card_transactions"
  description   = "Lambda function for handling card transactions"
  package_type  = "Zip"
  handler = "handler.lambda_handler"
  source_path = "${path.module}/../card_transactions"

  runtime = "python3.8"
  memory_size = 512
  timeout = 120
  tags = {
    managedBy = "terraform"
    created_by = "ceedeliz"
    Inventory = "Lambda"
    project_version = "1.0.0"
    owner = "ceedeliz@gmail.com"
    repo = "https://gitlab.com/ceedeliz_117/back-challenge"
    team = "holiz"
  }
}

### TERRAFORM VARIABLES DECLARATION ###
variable "env" {}

######## OUTPUTS #######
########output "card_transactions_lambda_version" {
########  value = module.card_transactions_lambda.version
########}
